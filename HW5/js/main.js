let firstName = prompt('First name: ');
let lastName = prompt('Last name: ');
let birthday = prompt('Birthday: ');
let newUser;

function createNewUser(firstName, lastName, birthday) {
    newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        },
        getAge() {
            let arrDate = birthday.split(".");
            let dd = arrDate[0];
            let mm = arrDate[1];
            mm--;
            let yy = arrDate[2];
            let now = new Date();
            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            let dateBirthday = new Date(yy, mm, dd); //Дата рождения
            let dateBirthdayNow = new Date(today.getFullYear(), dateBirthday.getMonth(), dateBirthday.getDate());
            let age = today.getFullYear() - dateBirthday.getFullYear();
            if (today < dateBirthdayNow) {
                age = age - 1;
            }
            return age;
        },
        getPassword() {
            let arrDate = birthday.split(".");
            return this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + arrDate[2];

        }
    };
    return newUser;
}

createNewUser(firstName, lastName, birthday);

console.log(newUser);
console.log("Login: \t\t" + newUser.getLogin());
console.log("Age: \t\t" + newUser.getAge());
console.log("Password: \t" + newUser.getPassword());

