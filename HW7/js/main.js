let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let arr2 = ['1', '2', '3', 'sea', 'user', 23];

function showListItems(arr) {
    document.write(`<ul>`);
    arr.map(function (item) {
        document.write(`<li>`);
        document.write(item);
        document.write(`</li>`);
    });
    document.write(`</ul>`);
}

showListItems(arr);
showListItems(arr2);
