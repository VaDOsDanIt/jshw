let arr = ['hello', 'world', 23, '23', true, null];

function filterBy(arr, type) {
    let newArr = [];
    arr.forEach(function (item) {
        if (typeof (item) !== type) newArr.push(item);
    });

    return newArr;
}

console.log(filterBy(arr, "number"));
console.log(filterBy(arr, "string"));
console.log(filterBy(arr, "boolean"));
console.log(filterBy(arr, "object"));
console.log('\n forEach используется для перебора массива.');
