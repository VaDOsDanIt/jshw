const passEnter = document.querySelector('.password-enter');
const passRepeat = document.querySelector('.password-repeat');
const buttonEye = document.querySelector('.icon-password');
const buttonEyeRepeat = document.querySelector('.icon-password-repeat');
const buttonAccept = document.querySelector('.btn');
const secondLabel = document.querySelector('.second-label');
const span = document.createElement('span');

buttonEye.addEventListener('click', changeIcon);
buttonEyeRepeat.addEventListener('click', changeIcon);
buttonAccept.addEventListener('click', onButtonClick);

function changeIcon(e) {
    e.target.classList.toggle('fa-eye');
    e.target.classList.toggle('fa-eye-slash');

    if (passEnter.type === "password") {
        passEnter.type = "text";
    } else {
        passEnter.type = "password";
    }
}

function onButtonClick(e) {
    e.preventDefault();
    if (passEnter.value === passRepeat.value && passEnter.value.length > 0) {
        span.innerText = '';
        alert('You are welcome');
    } else {
        if (span.innerText === '') {
            span.classList.add('error-span');
            secondLabel.append(span);
            span.innerText = 'Нужно ввести одинаковые значения';
            span.style.color = 'red';
        }
    }
}

