let button;
const input = document.querySelector('.inputText');
const section = document.querySelector("section");
const span = document.createElement('span');
const error = document.querySelector('.error');
section.append(error);
section.prepend(span);

input.addEventListener('focus', onMouseOver);
input.addEventListener('blur', onMouseOut);


function onClick() {
    span.innerText = "";
    input.value = "";
    error.innerText = "";
    input.style.border = "1px solid gray";
    button.remove();
}

function onMouseOver() {
    input.style.border = "1px solid green";
    input.style.color = "green";

}

function onMouseOut() {
    input.style.border = "1px solid gray";
    if (+input.value.length > 0 && +input.value > 0) {
        span.innerHTML = `Текущая цена: ${input.value}`;
        if (document.querySelectorAll('button').length === 0) {
            createButton();
        }
            error.innerText = '';
    }
    if (+input.value < 0 || +input.value !== +input.value) {
        input.style.border = "1px solid red";
        input.style.color = "red";
        error.innerText = 'Please enter correct price.';
        if (document.querySelectorAll('button').length === 0) {
            createButton();
        }
        span.innerText = '';
    }
}

function createButton() {
    button = document.createElement('button');
    button.innerText = 'X';
    input.after(button);
    button.addEventListener('click', onClick);
}