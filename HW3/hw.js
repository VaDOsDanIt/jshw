let num1, num2, operation, result;

do {
    num1 = +prompt('Number 1')
} while (isNaN(+num1) || num1 === null);


do {
    userInputOperation = prompt('Operation');
} while (userInputOperation !== '*'
&& userInputOperation !== '/'
&& userInputOperation !== '+'
&& userInputOperation !== '-');

operation = userInputOperation;

do {
    num2 = +prompt('Number 2')
} while (isNaN(+num2) || num2 === null);

function mathCalc(num1, operation, num2) {
    switch (operation) {

        case '+':
            result = +num1 + +num2;
            return result;
            break;
        case '-':
            result = num1 - num2;
            return result;
            break;
        case '*':
            result = num1 * num2;
            return result;
            break;
        case '/':
            result = num1 / num2;
            return result;
            break;
    }

}


console.log(mathCalc(num1, operation, num2));


// Функции нужны для выполнения одних и тех же действий в разных местах программы.
// Для передачи в функцию каких-либо данных используются аргументы.